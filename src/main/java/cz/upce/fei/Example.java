package cz.upce.fei;

import cz.upce.fei.love.partner.Partner;

import java.util.Random;
import java.util.stream.IntStream;

/**
 * Entry Point
 * */
public class Example {
    /**
     * This seed should generate false, false, true to demonstrate example.
     * */
    private static final int EXAMPLE_SEED = 7567;
    /**
     * Max number of arguments.
     * */
    private static final int MAX = 10;

    public static void main(String[] args) {
        var generator = new Random(EXAMPLE_SEED);
        var partner = new Partner(generator);

        IntStream.range(0, MAX).forEach(no -> {
            var argument = partner.getArgument();
            argument.defence("I am sorry");
        });
    }
}
