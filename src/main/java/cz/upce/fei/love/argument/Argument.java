package cz.upce.fei.love.argument;

/**
 * Simple argument for partner (AbstractObject).
 * */
public interface Argument {
    /**
     * You can defence for this message.
     * */
    void defence(String message);
}
