package cz.upce.fei.love.argument;

/**
 * Bad argument for partner (RealObject).
 * */
public class BadArgument implements Argument{
    /**
     * Reason for argument.
     * */
    private final String reason;

    /**
     * Bad argument init.
     * @param reason for argument.
     * */
    public BadArgument(String reason) {
        this.reason = reason;
    }

    @Override
    public void defence(String message) {
        System.out.printf("%s... %s... I never forget...\n", reason, message);
    }
}
