package cz.upce.fei.love.argument;

/**
 * No argument for partner (NullObject).
 * */
public final class NoArgument implements Argument {
    @Override
    public void defence(String message) {
        //Do nothing
        System.out.printf("%s, But nothing happened.\n", message);
    }

    /**
     * @return the singleton Instance.
     * */
    public static NoArgument instance(){
        return NoArgumentHolder.NO_ARGUMENT;
    }

    /**
     * Singleton class.
     * */
    private NoArgument(){
    }

    /**
     * Holder for No argument to prevent lazy initialization.
     * */
    private static class NoArgumentHolder{
        public static final NoArgument NO_ARGUMENT = new NoArgument();
    }
}
