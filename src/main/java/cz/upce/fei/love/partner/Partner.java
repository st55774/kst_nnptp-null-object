package cz.upce.fei.love.partner;

import cz.upce.fei.love.argument.Argument;
import cz.upce.fei.love.argument.BadArgument;
import cz.upce.fei.love.argument.NoArgument;

import java.util.Random;

/**
 * Simple Partner telling you his feelings.
 * */
public class Partner {
    /**
     * Feeling generator
     * */
    private Random feelingGenerator;

    /**
     * Partner init.
     * @param feelingGenerator random generator
     * */
    public Partner(Random feelingGenerator) {
        this.feelingGenerator = feelingGenerator;
    }

    /**
     * Feel to free your feelings.
     *
     * @return Argument based on feelings.
     * */
    public Argument getArgument(){
        if (!feelingGenerator.nextBoolean())
            return new BadArgument("I am mad at you!");
        else
            //return null Bad way to do this
            return NoArgument.instance();
    }
}
